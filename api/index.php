<?php
require_once __DIR__.'/vendor/autoload.php';
require_once('../firstlevel/tie_to_server.php'); //добавляем файл настроек для данного местоположения сервера
require_once('../bibl/classes/db.php'); // добавляем класс работы с базами данных

$app = new Silex\Application();

$app['debug'] = MY_DEBUG;// true - показывать ошибки

$hand_debug = false;// прикладная отладка по урл типа https://api.thlaspi.com/users/code/.../news/10

if($hand_debug)
{
	$version = 1;
}
else
{
	// достаём версию из заголовка accept
	if(isset($_SERVER['HTTP_ACCEPT']))
	{
		$_ver = explode('version',$_SERVER['HTTP_ACCEPT']);
		// $_ver[1] что-то вроде'1.+json'
		if(isset($_ver[1]) && $_ver[1] != '')
		{
			$ver = explode('.',$_ver[1]);
			if(! ($version = filter_var($ver[0], FILTER_SANITIZE_NUMBER_INT)))
			{
				header("HTTP/1.x 404 Not Found");
				header("Status: 404 Not Found");
				return 'Not ver integer';
			}
		}
		else
		{
			header("HTTP/1.x 404 Not Found");
			header("Status: 404 Not Found");
			return 'Not ver0';
		}
	}
	else
	{
		header("HTTP/1.x 404 Not Found");
		header("Status: 404 Not Found");
		return 'Not _ver1';
	}
}

//объявляем константы основного сайта, нужные для работы его классов
$_abspath = explode('api',dirname(__FILE__));
define('ABSPATH', $_abspath[0]);
define('BIBL', 'bibl');

$app['db'] = DB::getMySQL();//создаём объект для работы с бд




$app->get('/gardens/{garden_id}/plants', function(Silex\Application $app, $garden_id) use ($version)
// список растений в саду
{
	include_once __DIR__.'/Models/v'.$version.'/Plants.php';
    $fqcn = '\\Models\\v'.$version.'\\Plants';
	$plants = new $fqcn($app['db']);
	$result = $plants->get_plants($garden_id);
	if(isset($result['error_code']) && $result['error_code'] != 200)
	{
		$app->abort($result['error_code']);
	}
	else
	{
		return json_encode($result);
	}
})
->assert('garden_id', '\d+');



$app->get('/users/code/{synchr_code}/gardens/ids', function(Silex\Application $app, $synchr_code) use ($version)
// список id садов пользователя
{
	include_once __DIR__.'/Models/v'.$version.'/Gardens.php';
	include_once __DIR__.'/Models/v'.$version.'/Users.php';
	// получаем prinadlezhnost пользователя по его $synchr_code
    $fqcn = '\\Models\\v'.$version.'\\Users';
	$user_id = $fqcn::id_by_synchr_code($synchr_code);
	if(!$user_id)
	{
		$app->abort(404);
	}
	else
	{
		$fqcn = '\\Models\\v'.$version.'\\Gardens';
		$gardens = new $fqcn($app['db']);
		$result = $gardens->get_gardens_ids($user_id);
		if(isset($result['error_code']) && $result['error_code'] != 200)
		{
			$app->abort($result['error_code']);
		}
		else
		{
			return json_encode($result);
		}
	}
});


$app->get('/users/code/{synchr_code}/categories', function(Silex\Application $app, $synchr_code) use ($version)
// список категорий пользователя
{
	include_once __DIR__.'/Models/v'.$version.'/Category.php';
	include_once __DIR__.'/Models/v'.$version.'/Users.php';
	// получаем prinadlezhnost пользователя по его $synchr_code
    $fqcn = '\\Models\\v'.$version.'\\Users';
	$user_id = $fqcn::id_by_synchr_code($synchr_code);
	if(!$user_id)
	{
		$app->abort(404);
	}
	else
	{
		$fqcn = '\\Models\\v'.$version.'\\Category';
		$category = new $fqcn($app['db']);
		$result = $category->get_categories($user_id);
		if(isset($result['error_code']) && $result['error_code'] != 200)
		{
			$app->abort($result['error_code']);
		}
		else
		{
			return json_encode($result);
		}
	}
});


$app->get('/users/code/{synchr_code}/news/{count}', function(Silex\Application $app, $synchr_code, $count) use ($version)
// список новостей пользователя
{
	include_once __DIR__.'/Models/v'.$version.'/News.php';
	include_once __DIR__.'/Models/v'.$version.'/Users.php';
	// получаем prinadlezhnost пользователя по его $synchr_code
    $fqcn = '\\Models\\v'.$version.'\\Users';
	$user_id = $fqcn::id_by_synchr_code($synchr_code);
	if(!$user_id)
	{
		$app->abort(404);
	}
	else
	{
		$fqcn = '\\Models\\v'.$version.'\\News';
		$news = new $fqcn($app['db']);
		$result = $news->get_news($user_id, $count);
		if(isset($result['error_code']) && $result['error_code'] != 200)
		{
			$app->abort($result['error_code']);
		}
		else
		{
			return json_encode($result);
		}
	}
})
->assert('count', '\d+');


$app->get('/users/code/{synchr_code}/articles/{count}', function(Silex\Application $app, $synchr_code, $count) use ($version)
// список статей пользователя
{
	include_once __DIR__.'/Models/v'.$version.'/Articles.php';
	include_once __DIR__.'/Models/v'.$version.'/Users.php';
	// получаем prinadlezhnost пользователя по его $synchr_code
    $fqcn = '\\Models\\v'.$version.'\\Users';
	$user_id = $fqcn::id_by_synchr_code($synchr_code);
	if(!$user_id)
	{
		$app->abort(404);
	}
	else
	{
		$fqcn = '\\Models\\v'.$version.'\\Articles';
		$articles = new $fqcn($app['db']);
		$result = $articles->get_articles($user_id, $count);
		if(isset($result['error_code']) && $result['error_code'] != 200)
		{
			$app->abort($result['error_code']);
		}
		else
		{
			return json_encode($result);
		}
	}
})
->assert('count', '\d+');


$app->post('/shop/code/{synchr_code}/checkout', function(Silex\Application $app, $synchr_code) use ($version)
// Принять заказ от сторонней площадки (например, с сайта)
{
	include_once __DIR__.'/Models/v'.$version.'/Shop.php';
	include_once __DIR__.'/Models/v'.$version.'/Users.php';
	// получаем prinadlezhnost пользователя по его $synchr_code
    $fqcn = '\\Models\\v'.$version.'\\Users';
	$user_id = $fqcn::id_by_synchr_code($synchr_code);
	if(!$user_id)
	{
		$app->abort(404);
	}
	else
	{
		$fqcn = '\\Models\\v'.$version.'\\Shop';
		$shop = new $fqcn($app['db']);
		$result = $shop->checkout($user_id);
		if(isset($result['error_code']) && $result['error_code'] != 200)
		{
			$app->abort($result['error_code']);
		}
		else
		{
			return json_encode($result);
		}
	}
});



$app->run();
