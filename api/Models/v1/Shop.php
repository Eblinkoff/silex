<?php
namespace Models\v1;

class Shop
{
	private $db;

	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function checkout($user_id)
	/** Получает информацию о заказе, обрабатывает и записывает в бд
	@return array
	Структура объекта с заказом
	$checkout->sourse = 'hortulus.ru';// откуда пришёл заказ
	$checkout->plants = array(
		0 => stdClass{
			'thlaspi_id': ...,
			'offer_id': ...,
			'count': ...,
			'discount': ...,
		},
		1 => ... и так далее по числу заказанных растений
	);// список купленных растений
	$checkout->contacts = array(
		'name' => $name,
		'email' => $email,
		'phone' => $phone,
		'comment' => $comment,
	);// список контактов покупателя
	*/
	{
		$db = \DB::getMySQL();
		if(! isset($_POST['checkout']))
		{
			return array('error_code' => 204);
		}
		$checkout_obj = json_decode($_POST['checkout']);
		if(! is_object($checkout_obj))
		{
			return array('error_code' => 204);
		}
		// объект есть, ок. Теперь разбираем его.
		if(! isset($checkout_obj->sourse) || ! isset($checkout_obj->plants) || ! isset($checkout_obj->contacts))
		{
			return array('error_code' => 204);
		}
		
		// так. Начинаем валидацию.
		include_once(ABSPATH . BIBL .'/functions.php');
		
		// sourse
		$sourse = self::_text_val($checkout_obj->sourse, 15, true);
		if($sourse == false)
		{
			return array('error_code' => 415);
		}
		

		// name
		if(! isset($checkout_obj->contacts->name) || $checkout_obj->contacts->name == '')
		{
			return array('error_code' => 204);
		}
		$name = self::_text_val($checkout_obj->contacts->name, 35, true);
		if($name == false)
		{
			return array('error_code' => 415);
		}
		
		// email
		if(! isset($checkout_obj->contacts->email) || $checkout_obj->contacts->email == '')
		{
			$email = '';
		}
		else
		{
			$email = self::_text_val($checkout_obj->contacts->email, 129, true);
			if($email == false)
			{
				return array('error_code' => 415);
			}
			if(! $email = filter_var($email, FILTER_SANITIZE_EMAIL))
			{
				return array('error_code' => 415);
			}
		}
		
		// phone
		if(! isset($checkout_obj->contacts->phone) || $checkout_obj->contacts->phone == '')
		{
			$phone = '';
		}
		else
		{
			$phone = self::_text_val($checkout_obj->contacts->phone, 20, true);
			if($phone == false)
			{
				return array('error_code' => 415);
			}
		}
		
		// comment
		if(! isset($checkout_obj->contacts->comment) || $checkout_obj->contacts->comment == '')
		{
			$comment = '';
		}
		else
		{
			$comment = self::_text_val($checkout_obj->contacts->comment, 200, true);
			if($comment == false)
			{
				return array('error_code' => 415);
			}
		}
		
		// adress
		if(! isset($checkout_obj->contacts->adress) || $checkout_obj->contacts->adress == '')
		{
			$adress = '';
		}
		else
		{
			$adress = self::_text_val($checkout_obj->contacts->adress, 200, true);
			if($adress == false)
			{
				return array('error_code' => 415);
			}
		}
	
		// plants
		if(! is_array($checkout_obj->plants))
		{
			return array('error_code' => 204);
		}
		
		$plants = [];
		foreach($checkout_obj->plants as $key => $plant)
		{
			if(!isset($plant->thlaspi_id) || !isset($plant->offer_id) || !isset($plant->count) || !isset($plant->discount))
			{
				return array('error_code' => 204);
			}
			if(! check_integer($plant->thlaspi_id) || ! check_integer($plant->offer_id) || ! check_integer($plant->count) || ! check_integer($plant->discount))
			{
				return array('error_code' => 415);
			}
			$gap = new \stdClass();
			$gap->plant_id = $plant->thlaspi_id;
			$gap->offer_id = $plant->offer_id;
			$gap->count = $plant->count;
			$gap->discount = $plant->discount;
			$plants[] = $gap;
		}
		// проверки закончены. У нас есть контактные данные в переменных и массив с объектами, каждый из которых - купленный товар.

		include_once(ABSPATH . BIBL .'/classes/Sales.php');
		$checkout_id = \Sales::add_new($user_id, $buyer_id, 1);// $checkout_id - id заказа из таблицы shop_order_checkout
		if(!$checkout_id)
		{
			return array('error_code' => 417);
		}
		// покупка растений - запись их в бд заказанных растений
		$order_checkout = $db->get_row('
			SELECT * 
			FROM `shop_order_checkout` 
			WHERE `order_number` = '.$checkout_id.' AND `delete` = 1
		');
		if($order_checkout == null)
		{
			return array('error_code' => 418);
		}

		foreach($plants as $plant)
		{
			if(! \Sales::buy_plant_by_o($order_checkout, $plant)) // "покупаем" товары
			{
				return array('error_code' => 420);
			}
		}
		
		// записываем контактные данные
		if(!$db->query('
			UPDATE `shop_buyer_data` 
			SET `email` = "'.$email.'", `phone` = "'.$phone.'", `name` = "'.$name.'", `adress` = "'.$adress.'", `comment` = "'.$comment.'" 
			WHERE porjadk_n = '.$buyer_id
		))
		{
			return array('error_code' => 421);
		}
		
		return array('order_id' => $checkout_id);
	}
	
	function _text_val($text,$max_lenght,$may_be_empty=true)
	/** Проверяет переменную на string. 
	* @string param $text
	* @int param $max_lenght - максимальная длина строки
	* @boolean param $may_be_empty - если true (по умолчанию), то '' тоже проходит. Если false - '' не считается проходным и проходят только строки
	* @int return - проверенное значение $text или false, в случае непрохождения
	*/
	{
		if($text == '' && $may_be_empty)
		{
			return '';
		}
		
		if($text == '' && !$may_be_empty)
		{
			return false;
		}
		
		if(is_string($text))
		{
			if($text = filter_var($text, FILTER_SANITIZE_STRING))
			{
				if(mb_strlen($text,'utf-8') > $max_lenght){
					return mb_substr($text,0,$max_lenght,'utf-8');
				}else{
					return $text;
				}
			}else{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
}