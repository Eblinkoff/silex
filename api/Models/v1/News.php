<?php
namespace Models\v1;

class News
{
	private $db;

	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function get_news($prinadlezhnost, $count)
	/** отдаёт массив с новостями пользователя с данным $prinadlezhnost
	* @param int $prinadlezhnost - id пользователя, для которого нужно отдать категории
	* @param int $count - количество последних новостей, которые надо отдать. Если = 0 - отдаём все новости
	* @return array
	*/
	{
		$db = \DB::getMySQL();
		$limit = '';
		if($count > 0)
		{
			$limit = 'LIMIT '.$count;
		}
		$news = $db->get_results($db->prepare('
			SELECT porjadk_n AS `id`, fix, image_id, header, new AS `text`, thumbnail160x120path, full
			FROM `site_news`
			WHERE `prinadlezhnost` = "%d" AND `delete` = 1 AND `fix` <= NOW()
			ORDER BY `porjadk_n` DESC
			'.$limit.'
		',$prinadlezhnost),ARRAY_A);
		if($news == null || empty($news))
		{
			$news['error_code'] = 205;
			return $news;
		}
		else
		{
			foreach($news as &$one_new)
			{
				if($one_new['image_id'] != 0)
				{
					// убираем запрос к бд, т.к. картинка у новости одна и обращатся к бд не обязательно
					// $images = $db->get_results('
						// SELECT porjadk_n AS `id`, thumbnail160x120path AS thumbnail, imja AS full 
						// FROM `files`
						// WHERE porjadk_n = '.$one_new['image_id'].' AND `delete` = 1 AND `hide` = 0
						// ORDER BY `porjadk_n` DESC
						// LIMIT 1
					// ',ARRAY_A);
					// if($images != null)
					// {
						// $one_new['images'] = $images;
					// }
					$image_main = new \stdClass();
					$image_main->id = $one_new['image_id'];
					$image_main->thumbnail = $one_new['thumbnail160x120path'];
					$image_main->full = $one_new['full'];
					$one_new['images'] = array(0 => $image_main);
				}
				unset($one_new['image_id']);
			}
			return $news;
		}
	}
}