<?php
namespace Models\v1;

class Users
// класс работы с сущностью Пользователь
{
	public function id_by_synchr_code($synchr_code)
	/** отдаёт id пользователя по $synchr_code
	* @param int $synchr_code - код синхронизации
	* @return int $prinadlezhnost - id пользователя или false
	*/
	{
		$db = \DB::getMySQL();
		//получаем prinadlezhnost пользователя по его $synchr_code
		$rez_prinadlezhnost = $db->get_row($db->prepare('
			SELECT porjadk_n AS `id`
			FROM `agent_settings`
			WHERE `synchr_code` LIKE "%s"
		',$synchr_code));
		if($rez_prinadlezhnost == null || empty($rez_prinadlezhnost))
		{
			return false;
		}
		else
		{
			return $rez_prinadlezhnost->id;//id пользователя
		}
	}
	
}