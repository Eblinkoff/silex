<?php
namespace Models\v1;

class Category
{
	private $db;

	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function get_categories($prinadlezhnost)
	/** отдаёт массив с категориями пользователя с данным $prinadlezhnost
	* @param int $prinadlezhnost - id пользователя, для которого нужно отдать категории
	* @return array
	*/
	{
		$db = \DB::getMySQL();
		$categories = $db->get_results($db->prepare('
			SELECT porjadk_n AS `id`, imja AS `name`, title, description, keywords, level, parent_id, chpu, image_id, sort 
			FROM `plants_category`
			WHERE `prinadlezhnost` = "%d" AND `delete` = 1
			ORDER BY `sort`
		',$prinadlezhnost),ARRAY_A);
		if($categories == null || empty($categories))
		{
			$categories['error_code'] = 205;
			return $categories;
		}
		else
		{
			foreach($categories as &$category)
			{
				if($category['image_id'] != 0)
				{
					$images = $db->get_results('
						SELECT porjadk_n AS `id`, thumbnail160x120path AS thumbnail, imja AS full 
						FROM `files`
						WHERE porjadk_n = '.$category['image_id'].' AND `delete` = 1 AND `hide` = 0
						ORDER BY `porjadk_n` DESC
						LIMIT 1
					',ARRAY_A);
					if($images != null)
					{
						$category['images'] = $images;
					}
				}
				unset($category['image_id']);
			}
			return $categories;
		}
	}
}