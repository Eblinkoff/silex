<?php
namespace Models\v1;

class Articles
{
	private $db;

	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function get_articles($prinadlezhnost, $count)
	/** отдаёт массив с новостями пользователя с данным $prinadlezhnost
	* @param int $prinadlezhnost - id пользователя, для которого нужно отдать категории
	* @param int $count - количество последних новостей, которые надо отдать. Если = 0 - отдаём все новости
	* @return array
	*/
	{
		$db = \DB::getMySQL();
		$limit = '';
		if($count > 0)
		{
			$limit = 'LIMIT '.$count;
		}
		$articles = $db->get_results($db->prepare('
			SELECT porjadk_n AS `id`, fix, image_id, header, new AS `text`, thumbnail160x120path, full, article_id
			FROM `site_articles`
			WHERE `prinadlezhnost` = "%d" AND `delete` = 1 AND `fix` <= NOW()
			ORDER BY `porjadk_n` DESC
			'.$limit.'
		',$prinadlezhnost),ARRAY_A);
		if($articles == null || empty($articles))
		{
			$articles['error_code'] = 205;
			return $articles;
		}
		else
		{
			foreach($articles as &$one_article)
			{
				// картинки
				$images = $db->get_results($db->prepare('
					SELECT `id`, thumbnail, full, title 
					FROM `site_articles_images`
					WHERE article_id = '.$one_article['id'].' AND `prinadlezhnost` = %d
					ORDER BY `porjadk_n` DESC
				',$prinadlezhnost),ARRAY_A);
				if($images != null)
				{
					$one_article['images'] = $images;
				}
				// привязанные товары
				$tie_goods = $db->get_results('
					SELECT plant_id as thlaspi_id 
					FROM `dairy_plants_rel`
					WHERE dairy_id = '.$one_article['article_id']
				,ARRAY_A);
				if($tie_goods != null)
				{
					$one_article['tie_goods'] = $tie_goods;
				}
				unset($one_article['image_id']);
				unset($one_article['article_id']);
			}
			return $articles;
		}
	}
}