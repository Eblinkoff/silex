<?php
namespace Models\v1;

class Plants
{
	private $db;

	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function get_plants($garden_id)
	/** отдаёт массив с растениями данного сада в виде массива
	@return array
	*/
	{
		$db = \DB::getMySQL();
		$plants = [];
		include_once(ABSPATH . BIBL .'/classes/Synchronizations.php');// добавляем класс работы с синхронизацией основного сайта
		include_once(ABSPATH . BIBL .'/classes/Settings_user.php');
		if(!($is_allow_this_garden = \Synchronizations::is_allow_this_garden($garden_id)))
		//если синхронизация для данного сада запрещена - отказываем
		{
			$plants['error_code'] = 403;
			return $plants;
		}
		//получаем название сада
		include_once(ABSPATH . BIBL .'/classes/Garden.php');// добавляем класс работы с синхронизацией основного сайта
		$garden_name = \Garden::name_by_number($garden_id,$owner_number,$garden_obj);
		if($garden_name == '')
		{
			$plants['error_code'] = 204;
			return $plants;
		}
		//теперь выгружаем все растения из сада с id $garden_id
		$plants = $db->get_results('
			SELECT p.porjadk_n, p.porjadk_n AS `id`, p.plant_num AS article, p.imja AS rus_name, p.morph_plant, p.kol AS count, p.fix AS `date`, p.`comment`, p.`meta_comment`, p.`sort`, p.`lat_name`, p.`family`, p.`price`,p.`meta_sort_label`,p.`prinadlezhnost`, p.chpu, p.`hit`, p.`new`, p.`action` 
			FROM `plants` AS p 
			WHERE p.`place_pos` LIKE "'.$garden_name.'"'. \Synchronizations::type_upload($owner_number).' AND p.`delete` = 1 AND p.`status` = 1 AND prinadlezhnost = '.$owner_number.'
		',ARRAY_A);//ARRAY_N
		if($plants == null || empty($plants))
		{
			$plants['error_code'] = 204;
		}
		else
		{
			//добавляем картинки
			include_once(ABSPATH . BIBL .'/classes/Garden.php');// добавляем класс работы с синхронизацией основного сайта
			include_once(ABSPATH . BIBL .'/classes/Category.php');
			include_once(ABSPATH . BIBL .'/classes/Param.php');
			include_once(ABSPATH . BIBL .'/classes/Hide_param.php');
			include_once(ABSPATH . BIBL .'/classes/Offers.php');
			include_once(ABSPATH . BIBL .'/classes/Photo.php');
			include_once(ABSPATH . BIBL .'/classes/Settings_user.php');
			foreach($plants as &$plant)
			{
				$order_by = 'ORDER BY `organ` LIKE "Цветущий побег" DESC, `organ` LIKE "Цветок" DESC, `organ` LIKE "Общий вид" DESC, `organ` LIKE "Соцветие" DESC, `organ` DESC';
				if($plant->meta_sort_label == 1)
				{
					$order_by = 'ORDER BY `sorting_in_plant` ASC';
				}
				//  AND `hide` = 0 // посылаем скрытые фотографии тоже
				$images = $db->get_results('
					SELECT porjadk_n AS `id`, thumbnail160x120path AS thumbnail, imja AS full 
					FROM `files`
					WHERE private_porjadk_n = '.$plant['id'].' AND `delete` = 1
					'.$order_by.'
					LIMIT 5
				',ARRAY_A);
				if(!empty($images))
				{
					$plant['images'] = $images;
				}
				
				// добавляем привязку к категориям.
				if($categories_numbers = \Category::get_numbers($plant['id']))
				{
					$plant['categories'] = $categories_numbers;// $categories_numbers - это массив с номерами категорий
				}
				
				// добавляем список характеристик
				$param_numbers = '';
				if($param_numbers = \Param::get_numbers($plant['id'],$plant['prinadlezhnost']))
				{
					$plant['param'] = $param_numbers;// $param_numbers - это массив с номерами характеристик
				}
				
				// добавляем список скрытых характеристик
				if( \Settings_user::get_fast_settings('show_hide_par', $plant['prinadlezhnost']))
				{
					$hide_param_numbers = '';
					if($hide_param_numbers = \Hide_param::get_numbers($plant['id'],$plant['prinadlezhnost']))
					{
						$plant['hide_param'] = $hide_param_numbers;// $hide_param_numbers - это массив с номерами скрытых характеристик
					}
				}
				
				// добавляем список офферов
				if($offers = \Offers::get_numbers($plant['id'],$plant['prinadlezhnost']))
				{
					$plant['offers'] = $offers;
				}
				unset($plant['prinadlezhnost']);
				unset($plant['meta_sort_label']);
				// ещё комментарии
				if($plant['meta_comment'] != '')
				{
					$plant['comment'] = $plant['meta_comment'];
				}
				unset($plant['meta_comment']);
			}
		}
		return $plants;
	}
}