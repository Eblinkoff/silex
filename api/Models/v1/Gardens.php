<?php
namespace Models\v1;

class Gardens
{
	private $db;

	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function get_gardens_ids($prinadlezhnost)
	/** отдаёт массив с id садов пользователя с данным $synchr_code
	* @param int $prinadlezhnost - id пользователя, для которого нужно отдать номера садов
	* @return array
	*/
	{
		$db = \DB::getMySQL();
		$ids = [];
		//получаем prinadlezhnost пользователя по его $synchr_code
		// $rez_prinadlezhnost = $db->get_row($db->prepare('
			// SELECT porjadk_n AS `id`
			// FROM `agent_settings`
			// WHERE `synchr_code` LIKE "%s"
		// ',$synchr_code));
		// if($rez_prinadlezhnost == null || empty($rez_prinadlezhnost))
		// {
			// $ids['error_code'] = 204;
			// return $ids;
		// }
		// else
		// {
			// $prinadlezhnost = $rez_prinadlezhnost->id;//id пользователя
		// }
		//и теперь по полученной принадлежности отдаём id садов данного пользователя вида array(3) { [0]=> object(stdClass)#20 (1) { ["id"]=> string(2) "39" } [1]=> object(stdClass)#21 (1) { ["id"]=> string(2) "42" } [2]=> object(stdClass)#22 (1) { ["id"]=> string(2) "51" } } .
		$ids = $db->get_results($db->prepare('
			SELECT porjadk_n AS `id`
			FROM `gardens`
			WHERE `prinadlezhnost` = "%d"
		',$prinadlezhnost),ARRAY_A);
		if($ids == null || empty($ids))
		{
			$ids['error_code'] = 205;
			return $ids;
		}
		else
		{
			return $ids;
		}
	}
}